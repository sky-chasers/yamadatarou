import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { SlideDownAnimationService } from 'src/app/common/animations/slide-down.animation';
import { ButtonForm } from 'src/app/common/button/button-form';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: [
    './about-me.component.scss',
    './about-me.component.mobile.scss'
  ]
})
export class AboutMeComponent implements AfterViewInit, OnDestroy {

  @ViewChild('elementToAnimate')
  elementToAnimate!: ElementRef;

  constructor(private slideDownAnimationService: SlideDownAnimationService) {
  }

  ngOnDestroy(): void {
    this.slideDownAnimationService.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.slideDownAnimationService.applySlideDownAnimation(this.elementToAnimate);
  }
  
  public get buttonForm(): typeof ButtonForm {
    return ButtonForm; 
  }
  
}
