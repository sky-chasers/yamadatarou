export class WorkExperience {
    public readonly year: number;
    public readonly company: string;
    public readonly position: string;

    constructor(year: number, company: string, position: string) {
        this.year = year;
        this.company = company;
        this.position = position;
    }

    public getYear() {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        
        if(this.year === currentYear) {
            return "now";
        }

        return this.year;
    }
}