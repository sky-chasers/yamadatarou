import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { WorkExperience } from './work-experience';
import { Subscription } from 'rxjs';
import { SlideDownAnimationService } from '../../common/animations/slide-down.animation';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: [
    './work-experience.component.scss',
    'work-experience.component.mobile.scss'
  ]
})
export class WorkExperienceComponent implements AfterViewInit, OnDestroy {

  @ViewChild('elementToAnimate')
  elementToAnimate!: ElementRef;

  constructor(private slideDownAnimationService: SlideDownAnimationService) {}

  public workExperiences = [
    new WorkExperience(2023, 'ライジングサンウェブサービス', 'シニア UI/UX デザイナー'),
    new WorkExperience(2021, '禅日本ロボティクス', 'プロダクトデザイナー'),
    new WorkExperience(2018, 'サクラテックソリューションズ', 'ウェブ/UI デザイナー'),
    new WorkExperience(2014, 'サムライスタイルアパレル', 'ウェブデザイナー')
  ];

  private subscriptions: Subscription[] = [];

  ngOnDestroy(): void {
    this.slideDownAnimationService.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.slideDownAnimationService.applySlideDownAnimation(this.elementToAnimate);
  }

}
