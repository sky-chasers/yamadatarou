export class Work {
    public readonly title:string;
    public readonly description: string;
    public readonly imgSrc:string;

    constructor(title: string, description: string, imgSrc: string) {
        this.title = title;
        this.description = description;
        this.imgSrc = imgSrc;
    }

    public getImgPath() {
        return 'assets/my-works/' + this.imgSrc;
    }
}