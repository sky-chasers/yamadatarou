import { Component } from '@angular/core';
import { Work } from './work';

@Component({
  selector: 'app-my-works',
  templateUrl: './my-works.component.html',
  styleUrls: [
    './my-works.component.scss',
    './my-works.component.mobile.scss'
  ]
})
export class MyWorksComponent {

  public works = [
    new Work('ウェブアートコンネクト', 'モバイルアプリケーション', '001.jpeg'),
    new Work('ウェブフューチャーラボ', 'ウェブサイト', '002.jpg'),
    new Work('ウェブプロジェクトアクセル', 'ワイヤーフレーム', '003.jpg'),
  ];

  public activeSlideIndex = 0;

}
