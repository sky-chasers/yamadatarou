import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { bootstrapBehance, bootstrapLinkedin, bootstrapDribbble, bootstrapInstagram } from '@ng-icons/bootstrap-icons';
import { NgIconsModule } from '@ng-icons/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ButtonComponent } from 'src/app/common/button/button.component';
import { EmailService } from 'src/app/common/email/email.service';
import { EnvironmentService } from 'src/app/common/environment/environment.service';
import { FooterComponent } from 'src/app/common/footer/footer.component';
import { NavbarComponent } from 'src/app/common/navbar/navbar.component';
import { AboutMeComponent } from '../about-me/about-me.component';
import { ContactComponent } from '../contact/contact.component';
import { HomeComponent } from '../home.component';
import { WorkExperienceComponent } from '../work-experience/work-experience.component';

import { MyWorksComponent } from './my-works.component';
import { ClipboardModule } from 'ngx-clipboard';

describe('MyWorksComponent', () => {
  let component: MyWorksComponent;
  let fixture: ComponentFixture<MyWorksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        HomeComponent,
        AboutMeComponent,
        WorkExperienceComponent,
        ContactComponent,
        MyWorksComponent,
        ButtonComponent,
        FooterComponent,
        NavbarComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ClipboardModule,
        AlertModule.forRoot(),
        CarouselModule.forRoot(),
        PopoverModule.forRoot(),
        NgIconsModule.withIcons({ 
          bootstrapBehance, 
          bootstrapLinkedin, 
          bootstrapDribbble,
          bootstrapInstagram
        }),
      ],
      providers: [
        EmailService,
        EnvironmentService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
