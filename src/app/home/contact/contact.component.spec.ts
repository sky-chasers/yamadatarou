import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { bootstrapBehance, bootstrapLinkedin, bootstrapDribbble, bootstrapInstagram } from '@ng-icons/bootstrap-icons';
import { NgIconsModule } from '@ng-icons/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ButtonComponent } from 'src/app/common/button/button.component';
import { EmailService } from 'src/app/common/email/email.service';
import { EnvironmentService } from 'src/app/common/environment/environment.service';
import { FooterComponent } from 'src/app/common/footer/footer.component';
import { NavbarComponent } from 'src/app/common/navbar/navbar.component';
import { AboutMeComponent } from '../about-me/about-me.component';
import { HomeComponent } from '../home.component';
import { MyWorksComponent } from '../my-works/my-works.component';
import { WorkExperienceComponent } from '../work-experience/work-experience.component';

import { ContactComponent } from './contact.component';
import { ClipboardModule } from 'ngx-clipboard';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  let mockEmailService = jasmine.createSpyObj('EmailService', ['sendEmail']);
  let mockEnvironmentService = jasmine.createSpyObj('EnvironmentService', ['getEnvironment']);

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [ 
        HomeComponent,
        AboutMeComponent,
        WorkExperienceComponent,
        ContactComponent,
        MyWorksComponent,
        ButtonComponent,
        FooterComponent,
        NavbarComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ClipboardModule,
        AlertModule.forRoot(),
        CarouselModule.forRoot(),
        PopoverModule.forRoot(),
        NgIconsModule.withIcons({ 
          bootstrapBehance, 
          bootstrapLinkedin, 
          bootstrapDribbble,
          bootstrapInstagram
        }),
      ],
      providers: [
        { provide: EmailService, useValue: mockEmailService },
        { provide: EnvironmentService, useValue: mockEnvironmentService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('contact form', () => {
    it('should show the fist name field.', () => {
      const firstNameField = fixture.nativeElement.querySelector('.first-name-field input');

      expect(firstNameField).toBeTruthy();
    });
  });

  describe('clearSuccessMessage', () => {
    it('should clear successMessage', () => {
      component.successMessage = 'success';
      component.clearSuccessMessage();
      expect(component.successMessage).toEqual('');
    });
  });

  describe('clearErrorMessage', () => {
    it('should clear errorMessage', () => {
      component.errorMessage = 'error';
      component.clearErrorMessage();
      expect(component.errorMessage).toEqual('');
    });
  });

  describe('sendEmail', () => {

    it('should show error messages when the form is invalid.', () => {

    });

  });

});
