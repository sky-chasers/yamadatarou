import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonForm } from 'src/app/common/button/button-form';
import { EmailService } from '../../common/email/email.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: [
    './contact.component.scss',
    './contact.component.mobile.scss'
  ]
})
export class ContactComponent {

  public contactForm: FormGroup;
  public successMessage!: string;
  public errorMessage!: string;
  public submitText: string = '提出する';
  public contactButtonText: string = this.submitText;
  public isLoading: boolean = false;
  public showFieldErrors: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private emailService: EmailService
  ) {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  public get buttonForm(): typeof ButtonForm {
    return ButtonForm; 
  }

  public isFieldInvalid(fieldName: string) {
    return this.showFieldErrors && this.contactForm.controls[fieldName].invalid;
  }

  public onSubmit() {
    this.showFieldErrors = false;
    this.isLoading = true;
    this.contactButtonText = '送信中...';
    this.clearErrorMessage();
    this.clearSuccessMessage();
    
    if(this.contactForm.valid) {
      const name = this.contactForm.value.firstName + ' ' + this.contactForm.value.lastName; 
      const message = this.contactForm.value.message;
      const email = this.contactForm.value.email;

      this.emailService
      .sendEmail(name, message, email)
      .then(() => {this.onEmailSuccess();})
      .catch((error) => {this.onEmailFailed(error);})
      .finally(() => {this.resetForm();});
    } else {
      this.showFieldErrors = true;
      this.resetForm();
    }
  }

  public clearSuccessMessage() { 
    this.successMessage = '';
  }

  public clearErrorMessage() {
    this.errorMessage = '';
  }

  private resetForm() {
    this.isLoading = false;
    this.contactButtonText = this.submitText;
  }

  private onEmailFailed(error: any) {
    console.error('Error occurred while sending message: ', error);
    this.errorMessage = 'メッセージの送信中にエラーが発生しました。後で再試行してください。';
  }

  private onEmailSuccess() {
    this.successMessage = 'メッセージが正常に送信されました！';
    this.contactForm.reset();
  }

}
