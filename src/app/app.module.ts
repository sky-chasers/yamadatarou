import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutMeComponent } from './home/about-me/about-me.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { ButtonComponent } from './common/button/button.component';
import { WorkExperienceComponent } from './home/work-experience/work-experience.component';
import { HomeComponent } from './home/home.component';
import { MyWorksComponent } from './home/my-works/my-works.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ContactComponent } from './home/contact/contact.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './common/footer/footer.component';
import { NgIconsModule } from '@ng-icons/core';
import { bootstrapBehance } from '@ng-icons/bootstrap-icons';
import { bootstrapLinkedin } from '@ng-icons/bootstrap-icons';
import { bootstrapDribbble } from '@ng-icons/bootstrap-icons';
import { bootstrapInstagram } from '@ng-icons/bootstrap-icons';
import { EmailService } from './common/email/email.service';
import { EnvironmentService } from './common/environment/environment.service';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ClipboardModule } from 'ngx-clipboard';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { SlideDownAnimationService } from './common/animations/slide-down.animation';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutMeComponent,
    NavbarComponent,
    ButtonComponent,
    WorkExperienceComponent,
    MyWorksComponent,
    ContactComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ClipboardModule,
    AlertModule.forRoot(),
    CarouselModule.forRoot(),
    PopoverModule.forRoot(),
    CollapseModule.forRoot(),
    NgIconsModule.withIcons({ 
      bootstrapBehance, 
      bootstrapLinkedin, 
      bootstrapDribbble,
      bootstrapInstagram
    }),
  ],
  providers: [
    EmailService,
    EnvironmentService,
    SlideDownAnimationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
