import { Component, Input, OnInit } from '@angular/core';
import { ButtonForm } from './button-form';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input()text!:string;
  @Input()form!:ButtonForm;
  @Input()isDisabled:boolean = false;
  @Input()type:string = 'button';

}
