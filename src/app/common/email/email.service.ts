import { Injectable } from '@angular/core';
import { EnvironmentService } from '../environment/environment.service';
import emailjs from '@emailjs/browser';

@Injectable()
export class EmailService {

    private environment;

    constructor(private environmentService: EnvironmentService) {
        this.environment = this.environmentService.getEnvironment();

        emailjs.init(this.environment.EMAILJS.PUBLIC_KEY);
    }

    public sendEmail(recipientName: string, message: string, recipientEmail: string) {
        const templateParams = {
            name: recipientName,
            message: message,
            recipient_email: recipientEmail
        }

        return emailjs.send(
            this.environment.EMAILJS.SERVICE_ID,
            this.environment.EMAILJS.TEMPLATE_ID,
            templateParams
        );
    }

}
