import { ElementRef, Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable()
export class SlideDownAnimationService {

    public subscriptions: Array<Subscription> = [];

    public applySlideDownAnimation(elementToAnimate: ElementRef) {
        const options = {
            root: null,
            rootMargin: '0px',
            threshold: 0.2
          }
      
          const observer = new IntersectionObserver(this.handleIntersection, options);
          observer.observe(elementToAnimate.nativeElement);
      
          this.subscriptions.push(new Subscription(() => observer.disconnect()));
    }

    public unsubscribe() {
        this.subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        });
    }

    private handleIntersection(entries: IntersectionObserverEntry[]) {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            entry.target.classList.add('slide-down');
          } else {
            entry.target.classList.remove('slide-down');
          }
        });
    }

}
