import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { bootstrapBehance, bootstrapLinkedin, bootstrapDribbble, bootstrapInstagram } from '@ng-icons/bootstrap-icons';
import { NgIconsModule } from '@ng-icons/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AboutMeComponent } from 'src/app/home/about-me/about-me.component';
import { ContactComponent } from 'src/app/home/contact/contact.component';
import { HomeComponent } from 'src/app/home/home.component';
import { MyWorksComponent } from 'src/app/home/my-works/my-works.component';
import { WorkExperienceComponent } from 'src/app/home/work-experience/work-experience.component';
import { ButtonComponent } from '../button/button.component';
import { EmailService } from '../email/email.service';
import { EnvironmentService } from '../environment/environment.service';
import { NavbarComponent } from '../navbar/navbar.component';

import { FooterComponent } from './footer.component';
import { ClipboardModule } from 'ngx-clipboard';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        HomeComponent,
        AboutMeComponent,
        WorkExperienceComponent,
        ContactComponent,
        MyWorksComponent,
        ButtonComponent,
        FooterComponent,
        NavbarComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ClipboardModule,
        AlertModule.forRoot(),
        CarouselModule.forRoot(),
        PopoverModule.forRoot(),
        NgIconsModule.withIcons({ 
          bootstrapBehance, 
          bootstrapLinkedin, 
          bootstrapDribbble,
          bootstrapInstagram
        }),
      ],
      providers: [
        EmailService,
        EnvironmentService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
