import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: [
    './footer.component.scss',
    './footer.component.mobile.scss'
  ]
})
export class FooterComponent {

  public currentYear: number;
  public phoneNumber: string = '(012)-345-6789';
  public emailAddress: string = 'friendlyzenzen@gmail.com';

  constructor() {
    this.currentYear = new Date().getFullYear();
  }

}
