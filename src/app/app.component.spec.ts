import { TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { bootstrapBehance, bootstrapLinkedin, bootstrapDribbble, bootstrapInstagram } from '@ng-icons/bootstrap-icons';
import { NgIconsModule } from '@ng-icons/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './common/button/button.component';
import { EmailService } from './common/email/email.service';
import { EnvironmentService } from './common/environment/environment.service';
import { FooterComponent } from './common/footer/footer.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { AboutMeComponent } from './home/about-me/about-me.component';
import { ContactComponent } from './home/contact/contact.component';
import { HomeComponent } from './home/home.component';
import { MyWorksComponent } from './home/my-works/my-works.component';
import { WorkExperienceComponent } from './home/work-experience/work-experience.component';
import { ClipboardModule } from 'ngx-clipboard';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        HomeComponent,
        AboutMeComponent,
        WorkExperienceComponent,
        ContactComponent,
        MyWorksComponent,
        ButtonComponent,
        FooterComponent,
        NavbarComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ClipboardModule,
        AlertModule.forRoot(),
        CarouselModule.forRoot(),
        PopoverModule.forRoot(),
        NgIconsModule.withIcons({ 
          bootstrapBehance, 
          bootstrapLinkedin, 
          bootstrapDribbble,
          bootstrapInstagram
        }),
      ],
      providers: [
        EmailService,
        EnvironmentService
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title '山田太郎'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('山田太郎');
  });

});
