export const environment = {
    EMAILJS: {
        SERVICE_ID: import.meta.env["NG_APP_YAMADA_EMAILJS_SERVICE_ID"],
        TEMPLATE_ID: import.meta.env["NG_APP_YAMADA_EMAILJS_TEMPLATE_ID"],
        PUBLIC_KEY: import.meta.env["NG_APP_YAMADA_EMAILJS_PUBLIC_KEY"]
    }
};
